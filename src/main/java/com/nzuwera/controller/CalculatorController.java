package com.nzuwera.controller;

import com.nzuwera.dto.Request;
import com.nzuwera.dto.Results;
import com.nzuwera.service.ICalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Calculator rest endpoint
 */
@RestController
@RequestMapping(value = "/calculator")
@Validated
public class CalculatorController {

    @Autowired
    private ICalculatorService calculatorService;

    /**
     * Calculate endpoint accept
     * as input Request object
     *
     * @param request The Request Object to be processed
     * @return Result Object to be return
     */
    @PostMapping(value = "/")
    public Results calculate(@Valid @RequestBody Request request) {
        return calculatorService.calculate(request);
    }
}
