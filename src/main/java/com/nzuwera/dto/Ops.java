package com.nzuwera.dto;

/**
 * A: Addition
 * D: Division
 * S: Substraction
 * M: Multiplication
 */
public enum Ops {
    A,
    D,
    S,
    M
}
