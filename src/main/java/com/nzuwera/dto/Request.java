package com.nzuwera.dto;

import com.nzuwera.annotation.IsDouble;
import com.nzuwera.annotation.Operand;

import javax.validation.constraints.NotEmpty;

/**
 * Calculator input request
 */
public class Request {
    /**
     * First input number
     */
    @IsDouble(message = "numberA must be numeric value")
    @NotEmpty(message = "numberA is missing")
    private String numberA;
    /**
     * Second input number
     */
    @IsDouble(message = "numberB must be numeric value")
    @NotEmpty(message = "numberB is missing")
    private String numberB;
    /**
     * Arithmetic operation
     * A: Addition
     * S: Substraction
     * M: Multiplication
     * D: Division
     */
    @Operand
    @NotEmpty(message = "Operand is missing")
    private String operation;

    public Request() {
        // Empty Constructor
    }

    /**
     * Input request which will be calculated
     *
     * @param numberA   first input number
     * @param numberB   second input number
     * @param operation arithmetic operation to be performed on numberA and numberB
     */
    public Request(String numberA, String numberB, String operation) {
        this.numberA = numberA;
        this.numberB = numberB;
        this.operation = operation;
    }

    public String getNumberA() {
        return numberA;
    }

    public void setNumberA(String numberA) {
        this.numberA = numberA;
    }

    public String getNumberB() {
        return numberB;
    }

    public void setNumberB(String numberB) {
        this.numberB = numberB;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "Request{" +
                "numberA=" + numberA +
                ", numberB=" + numberB +
                ", operation=" + operation +
                '}';
    }
}
