package com.nzuwera.dto;

/**
 * Standard response
 */
public class ResponseObject {
    /**
     * Bolean status
     */
    private boolean status;
    /**
     * Response data
     */
    private Object data;
    /**
     * Response code
     */
    private String responseCode;
    /**
     * ErrorMessage
     */
    private String message;

    public ResponseObject() {
        // Empty Constructor
    }

    /**
     * Standard Response Object
     *
     * @param status       boolean status, True or False
     * @param data         Response Object
     * @param responseCode ErrorCode
     * @param message      ErrorMessage
     */
    public ResponseObject(boolean status, Object data, String responseCode, String message) {
        this.status = status;
        this.data = data;
        this.responseCode = responseCode;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseObject{" +
                "status=" + status +
                ", data=" + data +
                ", responseCode='" + responseCode + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
