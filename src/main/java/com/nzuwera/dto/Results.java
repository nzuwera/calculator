package com.nzuwera.dto;

/**
 * Calculator result
 */
public class Results {
    /**
     * Calculator response
     */
    private double response;

    public Results() {
        // Empty construction
    }

    /**
     * Result Object
     *
     * @param response response returned after calculations
     */
    public Results(double response) {
        this.response = response;
    }

    public double getResponse() {
        return response;
    }

    public void setResponse(double response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "Results{" +
                "response=" + response +
                '}';
    }
}
