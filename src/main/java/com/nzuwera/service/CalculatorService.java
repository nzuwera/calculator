package com.nzuwera.service;

import com.nzuwera.dto.Request;
import com.nzuwera.dto.Results;
import org.springframework.stereotype.Service;

@Service
public class CalculatorService implements ICalculatorService {

    /**
     * Calculate method accept
     *
     * @param request Object request
     * @return Result
     */
    @Override
    public Results calculate(Request request) {
        Results results;
        switch (request.getOperation()) {
            case "A":
                results = new Results(Double.parseDouble(request.getNumberA()) + Double.parseDouble(request.getNumberB()));
                break;
            case "M":
                results = new Results(Double.parseDouble(request.getNumberA()) * Double.parseDouble(request.getNumberB()));
                break;
            case "D":
                results = new Results(Double.parseDouble(request.getNumberA()) / Double.parseDouble(request.getNumberB()));
                break;
            case "S":
                results = new Results(Double.parseDouble(request.getNumberA()) - Double.parseDouble(request.getNumberB()));
                break;
            default:
                results = new Results(0);
        }
        return results;
    }
}
