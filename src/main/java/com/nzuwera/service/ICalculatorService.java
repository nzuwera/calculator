package com.nzuwera.service;

import com.nzuwera.dto.Request;
import com.nzuwera.dto.Results;

public interface ICalculatorService {

    Results calculate(Request request);
}
