package com.nzuwera.validator;

import com.nzuwera.annotation.IsDouble;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class InputValidator implements ConstraintValidator<IsDouble, String> {
    /**
     * Check if input is valid Double number
     *
     * @param value Value
     * @param context ConstraintValidatorContext
     * @return True or False
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            Double.parseDouble(value);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;

    }
}
