package com.nzuwera.validator;

import com.nzuwera.annotation.Operand;
import com.nzuwera.dto.Ops;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class OperationValidator implements ConstraintValidator<Operand, String> {

    /**
     * Check if operand is valid
     *
     * @param value Value
     * @param context ConstraintValidatorContext
     * @return True of false
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        for (Ops ops : Ops.values()) {
            if (ops.name().equals(value)) {
                return true;
            }
        }
        return false;

    }
}
