package com.nzuwera;

import com.nzuwera.controller.CalculatorControllerTest;
import com.nzuwera.service.CalculatorServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CalculatorControllerTest.class,
        CalculatorServiceTest.class
})
public class CalculatorApplicationTestSuite {
}
