package com.nzuwera.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs
public class CalculatorControllerTest {
    private double a, b, sum, division, substraction, multiplication;

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void TestAddition() throws Exception {
        mockMvc.perform(post("/calculator/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"numberA\": " + a + ",\"numberB\": " + b + ",\"operation\": \"A\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"response\":" + sum + "}"))
                .andDo(print())
                .andDo(document("CalculatorController/Addition",
                        requestFields(
                                fieldWithPath("numberA").description("First input number"),
                                fieldWithPath("numberB").description("Second input number"),
                                fieldWithPath("operation").description("Arithmetic operands. Supported operands A: Addition, S: Substraction, D: Division, M: Multiplication")
                        ),
                        responseFields(fieldWithPath("response").description("Calculator output"))
                ));
    }

    @Test
    public void TestDivision() throws Exception {
        mockMvc.perform(post("/calculator/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"numberA\": " + a + ",\"numberB\": " + b + ",\"operation\": \"D\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"response\":" + division + "}"))
                .andDo(print())
                .andDo(document("CalculatorController/Division",
                        requestFields(
                                fieldWithPath("numberA").description("First input number"),
                                fieldWithPath("numberB").description("Second input number"),
                                fieldWithPath("operation").description("Arithmetic operands. Supported operands A: Addition, S: Substraction, D: Division, M: Multiplication")
                        ),
                        responseFields(fieldWithPath("response").description("Calculator output"))
                ));
    }

    @Test
    public void TestMultiplication() throws Exception {
        mockMvc.perform(post("/calculator/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"numberA\": " + a + ",\"numberB\": " + b + ",\"operation\": \"M\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"response\":" + multiplication + "}"))
                .andDo(print())
                .andDo(document("CalculatorController/Multiplication",
                        requestFields(
                                fieldWithPath("numberA").description("First input number"),
                                fieldWithPath("numberB").description("Second input number"),
                                fieldWithPath("operation").description("Arithmetic operands. Supported operands A: Addition, S: Substraction, D: Division, M: Multiplication")
                        ),
                        responseFields(fieldWithPath("response").description("Calculator output"))
                ));
    }

    @Test
    public void TestSubstraction() throws Exception {
        mockMvc.perform(post("/calculator/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"numberA\": " + a + ",\"numberB\": " + b + ",\"operation\": \"S\"}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"response\":" + substraction + "}"))
                .andDo(print())
                .andDo(document("CalculatorController/Substraction",
                        requestFields(
                                fieldWithPath("numberA").description("First input number"),
                                fieldWithPath("numberB").description("Second input number"),
                                fieldWithPath("operation").description("Arithmetic operands. Supported operands A: Addition, S: Substraction, D: Division, M: Multiplication")
                        ),
                        responseFields(fieldWithPath("response").description("Calculator output"))
                ));
    }

    @Before
    public void setUp() {
        a = 6;
        b = 3;
        sum = a + b;
        multiplication = a * b;
        division = a / b;
        substraction = a - b;
    }
}