package com.nzuwera.service;

import com.nzuwera.dto.Ops;
import com.nzuwera.dto.Request;
import com.nzuwera.dto.Results;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorServiceTest {
    private double delta, sumResult, divResult, subResult, product;
    private String numberA, numberB;

    @Autowired
    private ICalculatorService calculatorService;

    @Test
    public void add() {
        Results sum = calculatorService.calculate(new Request(numberA, numberB, Ops.A.name()));
        Assert.assertEquals(sumResult, sum.getResponse(), delta);
    }

    @Test
    public void multiply() {
        Results results = calculatorService.calculate(new Request(numberA, numberB, Ops.M.name()));
        Assert.assertEquals(product, results.getResponse(), delta);
    }

    @Test
    public void substract() {
        Results product = calculatorService.calculate(new Request(numberA, numberB, Ops.S.name()));
        Assert.assertEquals(subResult, product.getResponse(), delta);
    }

    @Test
    public void divide() {
        Results product = calculatorService.calculate(new Request(numberA, numberB, Ops.D.name()));
        Assert.assertEquals(divResult, product.getResponse(), delta);
    }

    @Test
    public void unknownOperation() {
        Results results = calculatorService.calculate(new Request(numberA, numberB, "E"));
        Assert.assertEquals(0, results.getResponse(), delta);
    }

    @Before
    public void setUp() {
        numberA = "6";
        numberB = "3";
        sumResult = Double.parseDouble(numberA) + Double.parseDouble(numberB);
        product = Double.parseDouble(numberA) * Double.parseDouble(numberB);
        divResult = Double.parseDouble(numberA) / Double.parseDouble(numberB);
        subResult = Double.parseDouble(numberA) - Double.parseDouble(numberB);
    }
}